# driveMyBox Interview Assignment

<img src="https://i.pinimg.com/736x/d0/31/86/d03186fc2b77e5776eb1a659de8cf5c8.jpg" width="1150px" alt="TypeScript image"/>

- Create **Contacts** application using Angular (v >= **11**)
- Designs for the application can be seen under **assets** folder

<br/>

# 1. Overall explanation:

- Features can be improvised but any added or changed shouldn't deviate much from requirements listed bellow
- CRUD operation of contacts
- Views **All** that displays list of all available contacts and **Favorite** with corresponding favorite contacts
- Uppon clicking **hearts** icon contact is saved to Favorite list
- User can **search** both lists within search container
- User can add new contacts by pressing **plus icon**
- User can delete contacts by pressing **trash icon** and confirming on modal
- User can edit existing contact by clicking on **pen icon**
- Responsive design **(at least 3 layers)** 

<br/>

## 1.1 Technical requirements:

- Clean folder structure

<img src="https://pics.me.me/you-dont-ever-have-to-pass-parameters-if-every-variable-65694890.png" width="300" alt="Not all in main?"/> 

- Minimize code duplication (DRY technique)
- Write as few lines as possible
- Use appropriate naming conventions
- Encapsulate logic
- Avoid deep nesting
- Avoid long lines
- Leave comments and prioritize components/modules

<img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT1yuovnBzjDIJ3KRxVw43-KCnd2b9f256ebRHit2BxR_QI-91VDx8rSLMkqljRJEqLng4&usqp=CAU" width="300" alt="Not all in main?"/> 

- Use localStorage for data (single source of truth)
- Do not use any unecessary library 
- Write custom CSS
- Write Unit tests

<img src="https://pbs.twimg.com/media/EKkPagPXkAA__Qo.jpg" width="300" alt="Why unit tests?"/>


## 1.2 Bonus (optional):

- Use [Akita](https://www.npmjs.com/package/@datorama/akita) or [NgRx](https://ngrx.io/)
- Use [ESlint](https://eslint.org/) for identifying, reporting on patters and maintaing code quality with ease

<br/>

# 2. Submitting assignment

- Completed asgginment must be publicly visible on **Github** or **Bitbucket**

<img src="https://i.chzbgr.com/full/9295342592/h4ABC9C49/without-doing-a-pull-from-git-to-get-the-latest-updates-ialso-like-to-live-dangerously-quickmemecom" width="300" alt="VSC joke"/>

- For any questions regarding assignment feel free to contact **david.ceranic@nth.ch**

```c#
while(true) {
    Console.WriteLine("We wish you happy coding :) ");
}
```

<br><br>

**Copyright (c) 2019-2022 DriveMybox GmbH**